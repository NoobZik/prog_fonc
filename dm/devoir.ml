(* ************************************************************************** *)
(*                                                                            *)
(*                                                        :::      ::::::::   *)
(*   devoir.ml                                          :+:      :+:    :+:   *)
(*                                                    +:+ +:+         +:+     *)
(*   By: NoobZik <rakib.hernandez@gmail.com>        +#+  +:+       +#+        *)
(*                                                +#+#+#+#+#+   +#+           *)
(*   Created: 2019/04/26 14:25:19 by NoobZik           #+#    #+#             *)
(*   Updated: 2019/04/26 14:26:23 by NoobZik          ###   ########.fr       *)
(*                                                                            *)
(* ************************************************************************** *)

type eb = V of int | TRUE | FALSE | AND of eb * eb | OR of eb * eb
          | XOR of eb* eb | NOTof eb;;
