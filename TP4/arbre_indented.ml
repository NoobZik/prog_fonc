(**
 * Définition d'un nouveau type arbre.
 * Vide pour dire qu'un arbre est Vide
 * Noeud pour dire que l'arbre est non vide. Un noeud est constitué d'un entier
 * et deux sous arbre qui sont des arbre (cf spec de choppy)
*)

type arbre = Vide | Noeud of int * arbre * arbre ;;

(* Fonction appartient
 * Cette fonction retourne un boolean si la valeur donnée en argument "entier"
 * est dans l'arbre donnée en argument "arbre".
 *
 * Le match est perçu comme un switch case. Il y a deux cas à tester :
 * - Si l'arbre est vide (Définit par Vide).
 * - Dans le cas contraire, un Noeud qui contient un entier, 2 arbres.
 *
 * Cas de base :
 * Si l'arbre est vide alors on renvoie false.
 *
 * Cas de réccurence :
 * On prends le soin de préciser que x est un entier comme convenue dans la
 * définition d'un noeud
 *
 * Si x est inf à l'entier, alors on parcours l'arbre gauche.
 * L'arbre droit dans le cas contraire
 * Si x est égale à l'entier, on renvoie true.
 *
 * @param  arbre   Représente un arbre
 * @param  entier  L'entier à rechercher.
 * @return boolean -true si l'arbre contient bien l'entier donnée par entier.
 *                 -false dans les cas contraire.
 *
 * Cette fonction si elle est correct dans ocaml, doit retourner la
 * ligne suivante :
 * val appartient : arbre -> int -> bool = <fun>
 *
 *  *)
let rec appartient arbre entier =
  match arbre with
  |Vide->false
  |Noeud(x,g,d)->if(x==entier) then true
    else if (entier<x) then appartient g entier
    else appartient d entier;;

(* appartient (Noeud(5,Noeud(1,Vide,Vide),Noeud(8,Vide,Vide))) 8;; *)

(*
 *arbre_list
 *  *)

let rec arbre_liste arbre =
  match arbre with
  |Vide->[]
  |Noeud(x,g,d)->(arbre_liste g)@(x::(arbre_liste d));;

(* arbre_liste (Noeud(5,Noeud(1,Vide,Vide),Noeud(8,Vide,Vide))) ;; *)

(* Le @ veut dire concatener*)

let rec inserer_bas arbre entier =
  if( (appartient arbre entier) == false) then
    match arbre with
    |Vide->Noeud(entier,Vide,Vide)
    |Noeud(x,Vide,Vide)->if(entier<x) then Noeud(x,Noeud(entier,Vide,Vide),Vide)
      else Noeud(x,Vide,Noeud(entier,Vide,Vide))
    |Noeud(x,g,d)->if(entier<x) then Noeud(x,inserer_bas g entier,d)
      else Noeud(x,g,inserer_bas d entier)
  else arbre;;

(* inserer_bas (Noeud(5,Noeud(1,Vide,Vide),Noeud(8,Vide,Vide))) 9;; *)



let rec couper arbre entier =
  if( (appartient arbre entier) == false) then
    match arbre with
    |Vide->Vide,Vide
    |Noeud(x,g,d)->if(entier>x) then
        let (a,b)=(couper d entier) in (Noeud(x,g,a),b)
      else
        let (a,b)=(couper g entier) in (a,Noeud(x,b,d))
  else arbre,Vide;;

(* couper (Noeud(7,Noeud(4,Vide,Vide),Noeud(11,Noeud(9,Vide,Vide),Noeud(14,Vide,Vide)))) 5;; *)



let rec ajout_haut arbre entier =
  let (a,b)=(couper arbre entier) in Noeud(entier,a,b);;

(* ajout_haut (Noeud(7,Noeud(4,Vide,Vide),Noeud(11,Noeud(9,Vide,Vide),Noeud(14,Vide,Vide)))) 10;; *)

let rec list_arbre l1 =
  match l1 with
  |[]->Vide
  |t1::q1->ajout_haut (list_arbre q1) t1;;

let rec tri l1 =
  arbre_liste (list_arbre l1);;

tri [3;1;9;5];;
