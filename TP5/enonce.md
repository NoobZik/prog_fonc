Définir un type permettant d'exprimer des formules propositionelles :
  fp = Vrai, Faux, fp (ET) fp, fp (OU) fp, nofp, fp imp fp, fp iff fp, Var €N

  Une fp est en FNC si l'arbre est :
    - Pour Tout chemain de la racine à une feuille est de la forme : d'abord [0,n] (ET), puis
    soit vrai/faux/var pas de imp, pas de iff
