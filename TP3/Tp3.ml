(* ************************************************************************** *)
(*                                                                            *)
(*                                                        :::      ::::::::   *)
(*   Tp3.ml                                             :+:      :+:    :+:   *)
(*                                                    +:+ +:+         +:+     *)
(*   By: NoobZik <noobzik@pm.me>                    +#+  +:+       +#+        *)
(*                                                +#+#+#+#+#+   +#+           *)
(*   Created: 2019/03/12 17:22:02 by NoobZik           #+#    #+#             *)
(*   Updated: 2019/03/12 18:29:26 by NoobZik          ###   ########.fr       *)
(*                                                                            *)
(* ************************************************************************** *)

let rec concat l1 l2 = match l1 with
  | [] -> l2
  | e :: l -> e :: concat l l2;;

  let rec division liste=
      match liste with
      |[]->[],[]
      |a::[]->liste,[]
      |a::b::c->
      begin
          let (l1,l2)=division c in
          a::l1,b::l2
      end;;
  (* val division : 'a list -> 'a list * 'a list = <fun> *)

  let rec fusion liste1 liste2=
      match liste1,liste2 with
      |[],_->liste2;
      |_,[]->liste1;
      |t1::q1,t2::q2->
          if t1<t2 then
              t1::(fusion q1 liste2)
          else
              t2::(fusion liste1 q2);;
  (* val fusion : 'a list -> 'a list -> 'a list = <fun> *)

  let rec tri_fusion liste=
      match liste with
      |[]->[];
      |a::[]->liste;
      |_ ->
          begin
              let (liste1,liste2)=division liste in
              fusion (tri_fusion(liste1)) (tri_fusion(liste2));
          end;;
  (* val tri_fusion : 'a list -> 'a list = <fun> *)

  let rec partition liste pivot=
      match liste with
      |[]->[],[]
      |tt::qq->
          let (l1,l2)=partition qq pivot in
          if(tt<pivot) then (tt::l1,l2) else (l1,tt::l2);;
  (* val partition : 'a list -> 'a -> 'a list * 'a list = <fun> *)

  let rec tri_rapide liste=
      match liste with
      |[]->[];
      |a::ll->
          let (l1,l2)=partition ll a in
              (tri_rapide l1)@(a::(tri_rapide l2));;
  (* val tri_rapide : 'a list -> 'a list = <fun> *)

let rec cub n =
  if (n > 0) then (n * n * n) + cub(n-2)
  else 0;
