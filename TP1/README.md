Pour lancer le prompt :

```
ledit ocaml
```

Pour quitter ocalm

```
## quit;;
```

Les commentaires en ocaml sont sous cette forme

```ml
(* *)
```

Pour charger un fichier

# # use "nom de fichier.ml";;
