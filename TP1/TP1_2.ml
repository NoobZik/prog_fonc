(* ************************************************************************** *)
(*                                                                            *)
(*                                                        :::      ::::::::   *)
(*   TP1_2.ml                                           :+:      :+:    :+:   *)
(*                                                    +:+ +:+         +:+     *)
(*   By: NoobZik <noobzik@pm.me>                    +#+  +:+       +#+        *)
(*                                                +#+#+#+#+#+   +#+           *)
(*   Created: 2019/02/19 17:44:01 by NoobZik           #+#    #+#             *)
(*   Updated: 2019/02/19 18:20:13 by NoobZik          ###   ########.fr       *)
(*                                                                            *)
(* ************************************************************************** *)
let rec inter = function l -> function m ->
  if (m == l) then l
  else
  match l with
  | [] -> []
  | a :: t -> match m with
    | [] -> []
    | b::u -> if(a==b) then a :: inter t u
      else if (a<b) then inter t m
      else inter l u;;

let rec union = function l -> function m ->
  if (m == l) then l
  else match l with
    | [] -> m
    | a :: t -> match m with
      | [] -> []
      | b :: u -> if (a == b) then a :: union t u
      else if (a<b) then a :: union t m
      else b :: union l u

let rec appartient = function x -> function l ->
  match l with
  | [] -> false
  | a :: v -> if (x == a) then (true)
    else if (x < a) then false
    else appartient x v;;


let rec inter_2 = function l -> function m ->
  if (m == l) then l
  else
  match l with
  | [] -> []
  | a :: t -> match m with
    | [] -> []
    | b::u -> if(a==b) then a :: inter t u
      else if (u!=[]) then inter_2 l u
      else inter_2  t m;;
