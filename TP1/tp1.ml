(* ************************************************************************** *)
(*                                                                            *)
(*                                                        :::      ::::::::   *)
(*   tp1.ml                                             :+:      :+:    :+:   *)
(*                                                    +:+ +:+         +:+     *)
(*   By: NoobZik <noobzik@pm.me>                    +#+  +:+       +#+        *)
(*                                                +#+#+#+#+#+   +#+           *)
(*   Created: 2019/02/19 17:44:06 by NoobZik           #+#    #+#             *)
(*   Updated: 2019/02/19 17:44:07 by NoobZik          ###   ########.fr       *)
(*                                                                            *)
(* ************************************************************************** *)
let  rec fact x =
  if x = 1 then 1
  else x * fact(x-1);;

(* Par déclaration normale de fonction *)
(* Autrement dit une déclation de fonction *)

let rec fact_acc x acc =
  if x = 1 then acc
  else fact_acc (x-1) (acc * x);;

(* Fonction annonyme *)
let rec f = function x -> function acc ->
  if x = 1 then acc
  else f (x-1) (acc*x);;

(* let rec append l1 l2 *)
let rec append l1 l2 = match l1 with
  | [] -> l2
  | e :: l -> e :: append l l2;;

(* list reverse *)
let rec rev l1 = match l1 with
  | [] -> l1
  |  e :: l1 -> rev (l1) @ [e];;

(*a faire*)
let rec rev_acc l acc = match l with
  | [] -> acc
  |  e::l -> rev_acc l (e::acc)
  in rev_acc l [] ;;
